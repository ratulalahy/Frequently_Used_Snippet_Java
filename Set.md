### Array to Set
```java
Set<T> mySet = new HashSet<T>(Arrays.asList(someArray));
```

### Set to Array

https://www.geeksforgeeks.org/set-to-array-in-java/

### Subset
s1.containsAll(s2) — returns true if s2 is a subset of s1. (s2 is a subset of s1 if set s1 contains all of the elements in s2.)

### Union
s1.addAll(s2) — transforms s1 into the union of s1 and s2. (The union of two sets is the set containing all of the elements contained in either set.)

```java
Set<Type> union = new HashSet<Type>(s1);
union.addAll(s2);
```

### Intersection
s1.retainAll(s2) — transforms s1 into the intersection of s1 and s2. (The intersection of two sets is the set containing only the elements common to both sets.)

```java
Set<Type> intersection = new HashSet<Type>(s1);
intersection.retainAll(s2);
```

### Set Difference
s1.removeAll(s2) — transforms s1 into the (asymmetric) set difference of s1 and s2. (For example, the set difference of s1 minus s2 is the set containing all of the elements found in s1 but not in s2.)

```java
Set<Type> difference = new HashSet<Type>(s1);
difference.removeAll(s2);
```

### Set Clear
```java
set.clear()
```