# Traverse Map
https://www.geeksforgeeks.org/iterate-map-java/

#Retrieve all keys in arrayList
```java
Map<String, Integer> map = new HashMap<String, Integer>();
List<String> keys = new ArrayList(map.keySet())
```

#Retrieve all values in arraylist
```java
Map<String, Integer> map = new HashMap<String, Integer>();
List<Integer> values = new ArrayList(map.values());
```