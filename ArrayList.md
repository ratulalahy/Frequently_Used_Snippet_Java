### ArrayList to Array
```java
 String[] result = res.toArray(new String[res.size()]); //res is ArrayList of String
 ```
 This will only works for Object type. Not for primitive type.
 
 The below code will generate an error:
 ```java
 int[] result = res.toArray(new int[res.size()]); //res is ArrayList of String
 ```
 

### Array to ArrayList
```java
int[] ints = {1,2,3};
List<Integer> list = Arrays.stream(ints).boxed().collect(Collectors.toList());
```